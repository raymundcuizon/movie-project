<?php

Class Movie{

	private $dbconn;

	public function __construct(){
		// Include 'Database Config File'
		require_once( 'database.php' );
		$this->confPDO = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_PERSISTENT => false,);
		try {
			$this->dbconn = new PDO( "mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpass, $this->confPDO );
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
			exit();
		}
	}

	public function login($email, $password){
		$cred = $this->singleData($email, "status = 0 AND deleted = 0 AND email", "users");
		if (password_verify($password, $cred['password'])) {
			$_SESSION['userInfo'] = array(
				'id' => $cred['id'],
				'email' => $cred['email'], 
				'companyName' => $cred['company_name'], 
				'personIncharge' => $cread['person_incharge'],
				'role' => $cred['role'],
				'status' => $cred['status'],
				'deleted' => $cread['deleted'],
				'created' => $cred['created']);

			$data = ['username' => $cred['email'], 'company_name' => $cred['company_name'], 'action' => 'Login into system'];
			$this->log($data);
			
			header("location: index.php");
		} 
	}

	public function restriction($role, $adminPage, $clientPage){
		if($role === "1"){
			header("location: $adminPage");
		} else {
			header("location: $clientPage");
		}
	}

	public function update($table, $where, $data){
		try {
			$sets = [];
			$statement = "UPDATE $table SET";
			foreach ($data as $key => $value) {
				$sets[] = " " . $key . " = '" . $value . "'";
			}
			$statement .= implode(' , ', $sets);
			$statement .= $where;
			$statement_ready = $this->dbconn->prepare($statement);
			$statement_ready->execute(array_values($data));	
		} catch (PDOException $e) {
			print "ERROR!: ". $e->getMessage();
			exit();		
		}
	}

	public function log($data){
		try {
			$this->insert("logs", $data);
		} catch (PDOException $e) {
			print "ERROR!: ". $e->getMessage();
		}
	}

	public function insert($table, $data){
		try {
			$fields = implode(",", array_keys($data));
			$values = implode(",", array_fill(0, count($data), "?"));
			$statement = $this->dbconn->prepare("insert into $table ($fields) values ($values)");
			$statement->execute(array_values($data));
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
	}

	public function singleData($data, $where, $table) {
		$statement = $this->dbconn->prepare("select * from $table where $where = :data ");
		$statement->execute(array(':data' => $data));
		$d = $statement->fetch(PDO::FETCH_ASSOC);
		return $d;
	}

	public function select($table, $where, $data){
		try {
			$select = "SELECT * FROM $table $where";
			$statement = $this->dbconn->prepare($select);
			$statement->execute(array_values($data));
			while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
				$data[] = $row;
			}
			return $data;			
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
	}

	public function create_folder($folder){
		if (!is_dir($folder)) {
			mkdir($folder, 0755, true);
		}
	}

	// 
	public	function password_meter($data, $min_length, $max_length ) {
		if (strlen($data) >= $min_length) {
			if (strlen($data) <= $max_length) {
				$error = null;
			} else {
				$error = '<span class="label label-danger">Password must contain less than '.$max_length.' letters </span> &nbsp';
			}
		} else {
			$error = '<span class="label label-danger">Password must contain at least '.$min_length.' letters </span> &nbsp';
		}
		return $error;
	}

	public function required($name, $field = NULL){
		if (empty($name)) {
			$error = '<span class="label label-danger">'.$field." Must not be empty! </span> &nbsp";
		}
		return $error;
	}
	public function not_available($data, $field, $label){
		$check = $this->singleData($data, $field, "users");
		if($check){
			$error = '<span class="label label-danger">'.$label.' not available! </span> &nbsp';
		}
		return $error;

	}

	public function _randomFilename()
	{
		$string = substr(md5(uniqid(rand(1, 6))), 0, 8);
		return $string;
	}

}

?>