<?php
session_start();
include '../Movie.php';
$movie = new Movie();

$fileName = $movie->_randomFilename().".mp4"; // The file name
$fileTmpLoc = $_FILES["movie_url"]["tmp_name"]; // File in the PHP tmp folder
$fileType = $_FILES["movie_url"]["type"]; // The type of file it is
$fileSize = $_FILES["movie_url"]["size"]; // File size in bytes
$fileErrorMsg = $_FILES["movie_url"]["error"]; // 0 for false... and 1 for true
if (!$fileTmpLoc) { // if file not chosen
    echo "ERROR: Please browse for a file before clicking the upload button.";
    exit();
}
if(move_uploaded_file($fileTmpLoc, "../movie/tmp/".$_SESSION['userInfo']['id']."/$fileName")){
	$_SESSION['movieInfo'] = ['fileName' => $fileName];
    echo '<video width="820" height="740" controls><source src="movie/tmp/'.$_SESSION['userInfo']['id'].'/'.$fileName.'" type="video/mp4"><source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.</video>';
} else {
    echo "move_uploaded_file function failed";
}
?>