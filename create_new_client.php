<?php include 'header.php';?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h2 class="sub-header">Create new client</h2>
	<?php 


	if (isset($_POST['submit'])) {
		$errors = array();

		$email = $_POST['email'];
		$password = $_POST['password'];
		$clientName = $_POST['clientName'];
		$inCharge = $_POST['inCharge'];

		$errors  = $error_email =  $movie->required($email, "Email");
		$errors .= $error_password = $movie->required($password, "Password");
		$errors .= $error_clientName =  $movie->required($clientName, "Client name");
		$errors .= $error_inCharge =  $movie->required($inCharge, "");
		$errors .= $email_not_available = $movie->not_available($email, 'deleted = 0 AND email', 'Email');

		if(empty($errors)){
			// $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 10));
			$_SESSION['createUser'] = array("email" => $email, "password" => $password, "clientName" => $clientName, "inCharge" => $inCharge);
			header("location: create_new_client_review.php");
			// if($password_hash){
			// 	$data = array("email" => $email, "password" => $password_hash, "company_name" => $clientName, "person_incharge" => $inCharge, "role" => "0" , "deleted" => "0", "status" => "0");
			// 	$movie->insert("users",$data);
			// 	echo "OKAY";
			// }
		}
	}
	if($_SESSION['userInfo']['role'] === "1"){ ?>
	<div class="alert alert-warning" role="alert">
		<a href="#" class="alert-link">You have no access to this page.!</a>
	</div>
	<?php } else {
		?>
		<div class="table-responsive">
			<form method="post">
				<div class="form-group">
					<label for="clientName">Client name</label>
					<input type="etxt" class="form-control" id="clientName" name="clientName" value="<?= ($clientName)? $clientName : ''?>"	 placeholder="Client name">
					<?php if($error_clientName) { echo $error_clientName;}?>
				</div>
				<div class="form-group">
					<label for="inCharge">Name of the person incharge</label>
					<input type="text" class="form-control" id="inCharge" name="inCharge" value="<?= ($inCharge)? $inCharge : ''?>"	placeholder="Input name">
					<?php if($error_inCharge) { echo $error_inCharge;}?>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" id="exampleInputEmail1" name="email" value="<?= ($email)? $email : ''?>"	placeholder="Email">
					<?php if($error_email) { 
						echo $error_email;
					} elseif($email_not_available) { echo $email_not_available; }?>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<?php $random_pass = substr(str_shuffle('abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ1234567890'),rand(0,60-8),8);?>
					<input type="text" class="form-control" value="<?php echo $random_pass; ?>" id="exampleInputPassword1" name="password" placeholder="Password">
					<?php if($error_password) { echo $error_password;}?>
				</div>
				<button type="submit" name="submit" class="btn btn-primary">Create</button>
			</form>

		</div>
	</div>
	<?php } 
	include 'footer.php';

	?>