<?php include 'header.php';?>
<?php

if($_SERVER['HTTP_REFERER'] == "") {
	header("location: create_new_client.php");
}
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="container">
		<h3 class="text-muted">User Details</h3>
		<div class="row">
			<label class="col-sm-8 control-label">Email: <i><?= $_SESSION['createUser']['email']?></i></label>
		</div>
		<div class="row">
			<label class="col-sm-8 control-label">Password: <i><?= $_SESSION['createUser']['password']?></i></label>
		</div>
		<div class="row">
			<label class="col-sm-8 control-label">Client name: <i><?= $_SESSION['createUser']['clientName']?></i></label>
		</div>
		<div class="row">
			<label class="col-sm-8 control-label">Person Incharge: <i><?= $_SESSION['createUser']['inCharge']?></i></label>
		</div>
		<div class="row">
			<?php
			if(isset($_POST['submit'])){
				$password_hash = password_hash($_SESSION['createUser']['password'], PASSWORD_BCRYPT, array('cost' => 10));
				$data = array(
					"email" => $_SESSION['createUser']['email'], 
					"password" => $password_hash, 
					"company_name" => $_SESSION['createUser']['clientName'], 
					"person_incharge" => $_SESSION['createUser']['inCharge'], 
					"role" => "1" , 
					"deleted" => "0", 
					"status" => "0" );
				if($data){
					
					$dataLog = ['username' => $_SESSION['userInfo']['email'], 'company_name' => $_SESSION['userInfo']['companyName'], 'action' => 'Create new Client '];
					$movie->log($dataLog);

					$movie->insert("users",$data);
					unset($_SESSION['createUser']);
					header("location: create_new_client.php");
				}
			}
			?>
			<form method="post">
				<input class="btn btn-default" type="button" value="Back">
				<input class="btn btn-primary" type="submit" name="submit" value="Submit">
			</form>
		</div>
	</div>
</div>
<?php include 'footer.php';?>
