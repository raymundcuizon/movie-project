<?php 

include 'header.php';
// if($_SERVER['HTTP_REFERER'] == "") {
// 	header("location: index.php");
// }
$cred = $movie->singleData($_GET['id'], 'id', 'users');
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h2 class="sub-header">Edit Client </h2>
	<p>*note if you don't want to edit your password just leave it blank.</p>
	<?php 
	if (isset($_POST['submit'])) {
		$errors = array();

		$email = $_POST['email'];
		$password = $_POST['password'];
		$clientName = $_POST['clientName'];
		$inCharge = $_POST['inCharge'];
		$status = $_POST['status'];

		$errors  = $error_email =  $movie->required($email, "Email");
		$errors .= $error_clientName =  $movie->required($clientName, "Client name");
		$errors .= $error_inCharge =  $movie->required($inCharge, "");
		
		if($cred['email'] !== $email){
			$errors .=  $email_not_available = $movie->not_available($email, 'deleted = 0 AND email', 'Email');
		}

		if(empty($errors)){
			$_SESSION['editUser'] = array("id" =>$_GET['id'] ,"email" => $email, "password" => $password, "clientName" => $clientName, "inCharge" => $inCharge, "status" => $status);
			header("location: edit_client_review.php");
		}
	}
	if($_SESSION['userInfo']['role'] === "1"){ ?>
	<div class="alert alert-warning" role="alert">
		<a href="#" class="alert-link">You have no access to this page.!</a>
	</div>
	<?php } else {
		?>
		<div class="table-responsive">
			<form method="post">
				<div class="form-group">
					<label for="clientName">Client name</label>
					<input type="etxt" class="form-control" id="clientName" name="clientName" value="<?= $cred['company_name']?>" placeholder="Client name">
					<?php if($error_clientName) { echo $error_clientName;}?>
				</div>
				<div class="form-group">
					<label for="inCharge">Name of the person incharge</label>
					<input type="text" class="form-control" id="inCharge" name="inCharge" value="<?= $cred['person_incharge']?>" placeholder="Input name">
					<?php if($error_inCharge) { echo $error_inCharge;}?>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" id="exampleInputEmail1" name="email" value="<?= $cred['email']?>" placeholder="Email">
					<?php if($error_email) { echo $error_email;} elseif($email_not_available) { echo $email_not_available; }?>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<input type="text" class="form-control" value="<?php //echo $random_pass; ?>" id="exampleInputPassword1" name="password" placeholder="Password">
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="status" <?= ($cred['status'] === "1") ? "checked" : ''?> > Disable
					</label>
				</div>
				<button type="submit" name="submit" class="btn btn-primary">Create</button>
			</form>

		</div>
	</div>
	<?php } 
	include 'footer.php';

	?>