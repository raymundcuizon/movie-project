<?php include 'header.php';?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="container">
		<h3 class="text-muted">User Details</h3>
		<div class="row">
			<label class="col-sm-8 control-label">Email: <i><?= $_SESSION['editUser']['email']?></i></label>
		</div>
		<?php 
		if(!empty($_SESSION['editUser']['password'])){?>
		<div class="row">
			<label class="col-sm-8 control-label">Password: <i><?= $_SESSION['editUser']['password']?></i></label>
		</div>
		<?php }?>

		<div class="row">
			<label class="col-sm-8 control-label">Client name: <i><?= $_SESSION['editUser']['clientName']?></i></label>
		</div>
		<div class="row">
			<label class="col-sm-8 control-label">Person Incharge: <i><?= $_SESSION['editUser']['inCharge']?></i></label>
		</div>
		<div class="row">
			<label class="col-sm-8 control-label">Status: <i><?= ($_SESSION['editUser']['status'] === "on")? "Disable" : "Enable"?></i></label>
		</div>
		<div class="row">
			<?php
			if(isset($_POST['submit'])){
				if($_SESSION['editUser']['status'] === "on"){
					$status = "1";
				} else {
					$status = "0";
				}
				if(!empty($_SESSION['editUser']['password'])){
					$password_hash = password_hash($_SESSION['editUser']['password'], PASSWORD_BCRYPT, array('cost' => 10));
					$data = array(
						"email" => $_SESSION['editUser']['email'], 
						"password" => $password_hash, 
						"company_name" => $_SESSION['editUser']['clientName'], 
						"person_incharge" => $_SESSION['editUser']['inCharge'], 
						"role" => "1" , 
						"deleted" => "0", 
						"status" => $status );					
				} else {
					$data = array(
						"email" => $_SESSION['editUser']['email'],
						"company_name" => $_SESSION['editUser']['clientName'], 
						"person_incharge" => $_SESSION['editUser']['inCharge'], 
						"role" => "1" , 
						"deleted" => "0", 
						"status" => $status );					
				}

				if($data){
					$movie->update("users", "where id = {$_SESSION['editUser']['id']}" , $data);
					unset($_SESSION['editUser']);
					header("location: list_of_client.php");
				}
			}
			?>
			<form method="post">
				<input class="btn btn-default" type="button" value="Back">
				<input class="btn btn-primary" type="submit" name="submit" value="Submit">
			</form>
		</div>
	</div>
</div>
<?php include 'footer.php';?>