<?php include 'header.php';?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="table-responsive">
		<h2 class="sub-header">Latest projects</h2>
		<table class="table">
			<thead>
				<tr>
					<th>Company</th>
					<th></th>
					<th>Project name</th>
					<th>notification</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>A Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td>● new massage</td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
				<tr>
					<td>B Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td>● new massage</td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
				<tr>
					<td>C Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td></td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
				<tr>
					<td>D Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td>● new massage</td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
				<tr>
					<td>E Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td>● new massage</td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
				<tr>
					<td>F Inc.</td>
					<td><a href="#" class="thumbnail"><img src="..." alt="..."></a></td>
					<td>Project name On 1 st review</td>
					<td></td>
					<td><a href="#">Go to edit page</a></td>
				</tr>
			</tbody>
		</table>
		<nav>
			<div  class="text-center">
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>
<?php include 'footer.php';?>