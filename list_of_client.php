<?php 
include 'header.php';
$num_rec_per_page=10;
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
$start_from = ($page-1) * $num_rec_per_page; 
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="table-responsive">
		
			<div class="form-group">
				<label for=""><h2>List of client</h2></label>&nbsp&nbsp&nbsp&nbsp
				<button type="submit" class="btn btn-primary" onclick="location.href = 'create_new_client.php';">create new company</button>
			</div>
		
		<hr>
		<form class="form-inline" method="get">
			<div class="form-group">

				<div class="input-group">
					<input type="text" class="form-control" name="search" id="exampleInputAmount" placeholder="Input client name">
				</div>
			</div>
			<input type="submit" class="btn btn-primary">
		</form>
		<?php 
		if(isset($_GET)){ ?>
		<table class="table">
			<thead>
				<tr>
					<th>Company</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($movie->select("users", "where deleted = 0 AND role = 1 AND company_name LIKE '%$_GET[search]%'  LIMIT $start_from, $num_rec_per_page", "") as $value) { ?>
				<tr>
					<td><?php echo $value['company_name'];?></td>
					<td>
						<a href="project_list.php?id=<?= $value['id']?>"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></a> &nbsp&nbsp
						<a href="new_project.php?id=<?= $value['id']?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a> &nbsp&nbsp
						<a href="edit_client.php?id=<?= $value['id']?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
					</td>
				</tr>
				<?php }
				?>
			</tbody>
		</table>
		<?php } else { ?>
		<table class="table">
			<thead>
				<tr>
					<th>Company</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($movie->select("users", "where deleted = 0 AND role = 1 LIMIT $start_from, $num_rec_per_page", "") as $value) { ?>
				<tr>
					<td><?php echo $value['company_name'];?></td>
					<td>
						<a href="project_list.php?id=<?= $value['id']?>">Go to project list</a> | 
						<a href="new_project.php?id=<?= $value['id']?>">create new project</a>
					</td>
				</tr>
				<?php }
				?>
			</tbody>
		</table>
		<?php }

		$total_records = count($movie->select("users", "where deleted = 0 AND role = 1", ""));
		$total_pages = ceil($total_records / $num_rec_per_page);
		?>
		<nav>
			<div  class="text-center">
				<ul class="pagination">
					<li>
						<a href="list_of_client.php?page=1" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<?php
					for ($i=1; $i<=$total_pages; $i++) { 
						echo "<li><a href='list_of_client.php?page=".$i."'>".$i."</a> </li>"; 
					}; 
					?>
					<li>
						<a href="list_of_client.php?page=<?=$total_pages?>" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>
<?php include 'footer.php'; ?>