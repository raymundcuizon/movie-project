<?php
session_start();
include 'Movie.php';
$movie = new Movie();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">

  <title>Signin Template for Bootstrap</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <style type="text/css">
  body {
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #eee;
  }

  .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin .checkbox {
    font-weight: normal;
  }
  .form-signin .form-control {
    position: relative;
    height: auto;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 10px;
    font-size: 16px;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
  .form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  </style>

</head>

<body>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="account-wall">
          <form class="form-signin" method="post">
            <input type="text" class="form-control" name="email" value="<?= (isset($_POST['login']))? $_POST['email'] : ''?>" placeholder="Email" required autofocus>
            <input type="password" class="form-control" name="password" value="<?= (isset($_POST['login']))? $_POST['password'] : ''?>" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Sign in</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php 
  if(isset($_POST['login'])){
    $email = $_POST['email'];
    $password = $_POST['password'];
    $movie->login($email, $password);
  }
  ?>


<?php include 'footer.php';?>