<?php 
include 'header.php';
$data = ['username' => $_SESSION['userInfo']['email'], 'company_name' => $_SESSION['userInfo']['companyName'], 'action' => 'Logout into system'];
$movie->log($data);
session_destroy();
header("location: login.php");
?>