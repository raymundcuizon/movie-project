<?php 
include 'header.php';
$num_rec_per_page=10;
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
$start_from = ($page-1) * $num_rec_per_page; 
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="table-responsive">
		<?php 
		if(isset($_GET)){ ?>
		<table class="table">
			<thead>
				<tr>
					<th>Date</th>
					<th>Company name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($movie->select("logs", "LIMIT $start_from, $num_rec_per_page", "") as $value) { ?>
				<tr>
					<td><?php echo $value['created'];?></td>
					<td><?php echo $value['company_name'];?></td>
					<td><?php echo $value['username'];?></td>
					<td><?php echo $value['action'];?></td>
				</tr>
				<?php }
				?>
			</tbody>
		</table>
		<?php } else { ?>
		<table class="table">
			<thead>
				<tr>
					<th>Date</th>
					<th>Company name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($movie->select("users", "LIMIT $start_from, $num_rec_per_page", "") as $value) { ?>
				<tr>
					<td><?php echo $value['created'];?></td>
					<td><?php echo $value['company_name'];?></td>
					<td><?php echo $value['username'];?></td>
					<td><?php echo $value['action'];?></td>
				</tr>
				<?php }
				?>
			</tbody>
		</table>
		<?php }
		$total_records = count($movie->select("logs", "", ""));
		$total_pages = ceil($total_records / $num_rec_per_page);
		?>
		<nav>
			<div  class="text-center">
				<ul class="pagination">
					<li>
						<a href="logs.php?page=1" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<?php
					for ($i=1; $i<=$total_pages; $i++) { 
						echo "<li><a href='logs.php?page=".$i."'>".$i."</a> </li>"; 
					}; 
					?>
					<li>
						<a href="logs.php?page=<?=$total_pages?>" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>
<?php include 'footer.php'; ?>