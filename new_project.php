<?php 
include 'header.php';
$folder = $movie->create_folder("movie/tmp/".$_SESSION['userInfo']['id']);
$folder = $movie->create_folder("movie/projects/".$_GET['id']);
if(!$_POST){
    $files = glob("movie/tmp/".$_SESSION['userInfo']['id']."/{,.}*", GLOB_BRACE); 
    foreach($files as $file){ 
        if(is_file($file))
            unlink($file); 
    }
    unset($_SESSION['movieInfo']);
    unset($_SESSION['newProject']);
}

?>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
progress {
    background-color: #f3f3f3;
    border: 0;
    border-radius: 9px;
}
</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h2 class="sub-header">Client name: A Inc</h2>
     <?php 
        if (isset($_POST['submit'])) {
           // if(!empty($_FILES)){ 
            $_SESSION['newProject'] = ['review' => $_POST['review'], 'final' => $_POST['final'], 'finished' => $_POST['finished'], 'clientId' => $_GET['id']];
            header("location: new_project_review.php");
           // }      
        }
        ?>
	<div class="table-responsive">
		<div class="col-md-12">
			<form method="post">
				<div class="form-group" enctype="multipart/form-data">
					<label for="review">Review: </label>
					<select class="form-control" id="review" name="review">
						<option>1st</option>
						<option>2nd</option>
						<option>3rd</option>
					</select>
				</div>
				<div class="form-group">
					<label for="finall">Final: </label>
					<input type="checkbox" id="final" name="final">
				</div>
				<div class="form-group">
					<label for="finished">Finished: </label>
					<input type="checkbox" id="finished" name="finished">
				</div>
				<div class="form-group">
                    <div id="remove"></div>
                    <p id="status"></p>
                    <label for="movie_url">Video: </label>
                    <span class="btn btn-default btn-file">Browse
                       <input type="file" id="movie_url" name="movie_url" accept="video/mp4">
                   </span>
                   <progress id="progressBar" value="0" max="100" style="width:100%;" class=""></progress>
               </div>
               <div class="form-group">
                <input type="submit" name="submit" class="btn btn-primary" value="Create">
            </div>
        </form>
		</div>
	</div>
</div>


<script type="text/javascript">
/* Script written by Adam Khoury @ DevelopPHP.com */
/* Video Tutorial: http://www.youtube.com/watch?v=EraNFJiY0Eg */
function _(el){
 return document.getElementById(el);
}
var fileupload = $("#movie_url");
fileupload.on('change', function(){
 var file = _("movie_url").files[0];
 var formdata = new FormData();
 formdata.append("movie_url", file);
 var ajax = new XMLHttpRequest();
 ajax.upload.addEventListener("progress", progressHandler, false);
 ajax.addEventListener("load", completeHandler, false);
 ajax.addEventListener("error", errorHandler, false);
 $('#loader-icon').show();
 ajax.addEventListener("abort", abortHandler, false);
 ajax.open("POST", "ajax/uploadMovie.php");
 ajax.send(formdata);
});
function progressHandler(event){
        	// _("loaded_n_total").innerHTML = event.loaded+" bytes / "+event.total;
        	var percent = (event.loaded / event.total) * 100;
        	_("progressBar").value = Math.round(percent);
        	_("status").innerHTML = Math.round(percent) + "% Uploaded... please wait ";
        }
        function completeHandler(event){
        	_("status").innerHTML = event.target.responseText;
        	_("remove").innerHTML = "<button type='button' class='btn btn-danger' id='removeBtn'>remove</button>";
        	$('#loader-icon').hide();
        	_("progressBar").value = 0;
        }
        function errorHandler(event){
        	_("status").innerHTML = "Upload Failed";
        }
        function abortHandler(event){
        	_("status").innerHTML = "Upload Aborted";
        }
        </script>
        <?php include 'footer.';?>